"use strict"

const button = document.getElementById("btn");

button.addEventListener("click", async function (){
  const ip = await fetch("https://api.ipify.org/?format=json");
  const userIp = await ip.json();
  
  const positionResponse = await fetch(`http://ip-api.com/json/${userIp.ip}?lang=ru&fields=continent,country,region,city,district`);
  const infoLocation = await positionResponse.json();

  const block = document.getElementById("root");


  for(let[key, value] of Object.entries(infoLocation)){
    block.innerHTML += `<p>${key}:${value}</p>`
  }
});
